package net.junxure.common.pages;

import net.junxure.utils.PostgresStringComparator;
import net.junxure.utils.StringUtilsHelper;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class BasePage extends PageObject {

    protected final int headerIndex = 1;

    public void wait_for_element_to_disappear(By by) {
        waitFor(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public boolean contains_text_ignoring_case(String text) {
        List<WebElementFacade> elements = findAll(By.xpath(
                "/html/body//*[not(self::script)]/text()[contains(translate(.,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'" + text.toLowerCase() + "')]"));
        boolean isDisplayed = false;
        for (WebElementFacade element : elements) {
            if (element.isVisible()) {
                isDisplayed = true;
                break;
            }
        }
        return isDisplayed;
    }

    public void show_entries_per_page(
            WebElementFacade numberOfEntriesSelectBox,
            WebElementFacade conditionToWait,
            WebElementFacade showEntriesButton) {
        waitFor(ExpectedConditions.elementToBeClickable(conditionToWait));
        showEntriesButton.click();
        numberOfEntriesSelectBox.click();
        waitFor(ExpectedConditions.elementToBeClickable(conditionToWait));
    }

    /**
     * Common approach is to identify the table column index by specifying the table header as a parameter
     * So if header is changed, or column position is changed, we still have a ability of getting proper column index.
     * HeaderIndex parameter (1) is added, since array indexing starts with 0
     */
    public int get_column_index_by_column_name_from_table(String columnName, By listOfHeadersSelector) {
        int headerIndex = 1;
        List<String> getListOfHeaders = findAll(listOfHeadersSelector).
                stream().
                map(element -> element.getText().trim()).
                collect(Collectors.toList());
        return getListOfHeaders.indexOf(columnName) + headerIndex;
    }

    /**
     * Unique Header index retrieved from the "get_column_index_by_column_name_from_table" method
     * This approach gives us the ability to retrieve values for sorting from the specific column
     */
    public List<String> get_list_of_sorted_elements(String columnName, By listOfHeadersSelector) {
        List<String> get_list_of_sorted_elements = new ArrayList<>();
        int columnIndex = get_column_index_by_column_name_from_table(columnName, listOfHeadersSelector);
        List<WebElementFacade> listOfSortableItems = findAll(By.xpath("//tr/td[" + columnIndex + "]"));
        List<String> sortableText = new ArrayList<>();
        listOfSortableItems.forEach(cell -> sortableText.add(cell.getAttribute("innerText")));
        sortableText.stream().
                filter(StringUtils::isNotBlank).
                forEach(
                        sortableItem -> {
                            get_list_of_sorted_elements.add(sortableItem);
                        }
                );
        return get_list_of_sorted_elements;
    }

    public void verify_list_is_sorted_by_column_and_order_for_string_values(String sortOrder, List<String> actualListOfSortableItems) {
        List<String> listOfSortableStrings = StringUtilsHelper.remove_salutation(actualListOfSortableItems);

        switch (sortOrder) {
            case "ascending":
                assertThat(listOfSortableStrings).
                        as("List is not sorted properly in the ascending order").
                        isSortedAccordingTo(new PostgresStringComparator());
                break;
            case "descending":
                assertThat(listOfSortableStrings).
                        as("List is not sorted properly in the descending order").
                        isSortedAccordingTo(new PostgresStringComparator().reversed());
                break;
            default:
                throw new AssertionError("Incorrect sort criteria was supplied");
        }
    }

    public void verify_list_is_sorted_by_column_and_order_for_numbers(String sortOrder, List<String> actualListOfSortableItems) {
        
        List<Double> listOfSortableNumbers = StringUtilsHelper.remove_percentage_and_comma_sign(actualListOfSortableItems);

        switch (sortOrder) {
            case "ascending":
                assertThat(listOfSortableNumbers).
                        as("List is not sorted properly in the ascending order").
                        isSortedAccordingTo(Comparator.naturalOrder());
                break;
            case "descending":
                assertThat(listOfSortableNumbers).
                        as("List is not sorted properly in the descending order").
                        isSortedAccordingTo(Comparator.reverseOrder());
                break;
            default:
                throw new AssertionError("Incorrect sort criteria was supplied");
        }
    }

    public void verify_list_is_sorted_by_column_and_order_for_dates(String sortOrder, List<String> actualListOfSortableItems) {
        List<LocalDate> listOfSortableDates = StringUtilsHelper.convert_list_of_strings_to_list_of_dates(actualListOfSortableItems);

        switch (sortOrder) {
            case "ascending":
                assertThat(listOfSortableDates).
                        as("List is not sorted properly in the ascending order").
                        isSortedAccordingTo(Comparator.naturalOrder());
                break;
            case "descending":
                assertThat(listOfSortableDates).
                        as("List is not sorted properly in the descending order").
                        isSortedAccordingTo(Comparator.reverseOrder());
                break;
            default:
                throw new AssertionError("Incorrect sort criteria was supplied");
        }
    }

    public String get_visible_sorting_arrow_type(
            By defaultIcon,
            By ascendingIcon,
            By descendingIcon) {
        WebElementFacade defaultIcons = find(defaultIcon);
        WebElementFacade ascendingIcons = find(ascendingIcon);
        WebElementFacade descendingIcons = find(descendingIcon);

        if (defaultIcons.isVisible()) {
            return "default";
        } else if (ascendingIcons.isVisible()) {
            return "ascending";
        } else if (descendingIcons.isVisible()) {
            return "descending";
        } else
            throw new AssertionError("Sortable Icon is not visible");
    }

    /**
     * Each table in the various pages have different default sort columns and behavior.
     * For example, the default sorting for the Contacts page is the Contact Name in ascending order,
     * while the Manage Clients - Dashboard might have a different default column.
     * As a result, the steps required to perform sorting correctly varies from page to page,
     * and the elements need to be re-initialized after each page action in order to mitigate StaleElementReferenceException
     * errors. Hence the use of the By object rather than a WebElementFacade
     */
    public void sort_table(String sortCriteria,
                           By columnSortButton,
                           By defaultIcon,
                           By ascendingIcon,
                           By descendingIcon,
                           By conditionToWait
    ) {
        String visibleArrowType = get_visible_sorting_arrow_type(defaultIcon, ascendingIcon, descendingIcon);
        switch (sortCriteria) {
            case "descending":
                switch (visibleArrowType) {
                    case "descending":
                        return;
                    case "ascending":
                        find(columnSortButton).click();
                        return;
                    case "default":
                        find(columnSortButton).click();
                        wait_for_element_to_disappear(conditionToWait);
                        if (get_visible_sorting_arrow_type(defaultIcon, ascendingIcon, descendingIcon).equals("ascending")) {
                            find(columnSortButton).click();
                        }
                        return;
                }
            case "ascending":
                switch (visibleArrowType) {
                    case "descending":
                        find(columnSortButton).click();
                        return;
                    case "ascending":
                        return;
                    case "default":
                        find(columnSortButton).click();
                        wait_for_element_to_disappear(conditionToWait);
                        if (get_visible_sorting_arrow_type(defaultIcon, ascendingIcon, descendingIcon).equals("descending")) {
                            find(columnSortButton).click();
                        }
                }
        }
    }

    /**
     * Clicks WebElement using Javascript.
     * This should only be used in cases where the native Serenity/WebElementFacade click() does not work consistently
     */
    public void click_with_javascript(WebElement elementToClick) {
        evaluateJavascript("arguments[0].click();", elementToClick);
    }

    public void type_with_javascript(WebElement element, String text) {
        evaluateJavascript("arguments[0].value=" + text + ";", element);
    }

    /**
     * Focuses on WebElement using Javascript.
     * Should be used when it's necessary to scroll or focus on WebElement before interacting with it.
     */
    public void focus_on_element_by_element_name(String elementName) {
        evaluateJavascript("document.getElementsByName('" + elementName + "')[0].focus()");
    }

    protected void move_slider(WebElement slider, int pixels) {
        Actions move = new Actions(this.getDriver());
        Action action = move.dragAndDropBy(slider, pixels, 0).build();
        action.perform();
    }

    protected void move_to_element(WebElement element){
        Actions actions = new Actions(this.getDriver());
        actions.moveToElement(element);
        actions.perform();
    }

    /**
     * Scrolls so that the specified element is visible, using JavaScript.
     *
     * @param elementId The id of the element
     * @author Walter Finkbeiner
     */
    protected void scroll_to_element_with_id_using_js(String elementId) {
        this.getJavascriptExecutorFacade().executeScript(
                "document.getElementById('" + elementId + "').scrollIntoView(true);");
    }

    public void click_on_element_with_exact_text(String tag, String text) {
        clickOn(find(By.xpath("//" + tag + "[text()='" + text + "']")));
    }

}
