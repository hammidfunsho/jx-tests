package net.junxure.common.pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

@DefaultUrl("https://junxurecloud.com")
public class LoginPage extends BasePage {

    @FindBy(id = "UserName")
    private WebElementFacade usernameField;

    @FindBy(id = "Password")
    private WebElementFacade passwordField;

    @FindBy(xpath = ".//*[@class='btn btn-large']")
    private WebElementFacade loginButton;

    @FindBy(xpath = ".//*[contains(text(), 'Create Account')]")
    private WebElementFacade createAccountLink;

    @FindBy(xpath= "//input[contains(@name,'firstName')]")
    private WebElementFacade firstNameField;

    @FindBy(xpath= "//input[contains(@name,'lastName')]")
    private WebElementFacade lastNameField;

    @FindBy(xpath= "//input[contains(@name,'email')]")
    private WebElementFacade emailAddressField;

    @FindBy(xpath= "//input[contains(@name,'email2')]")
    private WebElementFacade emailAddressConfirmationField;

    @FindBy(xpath= "//input[contains(@name,'passwd2')]")
    private WebElementFacade passwordConfirmationField;

    @FindBy(xpath= "//button[@type='submit']")
    private WebElementFacade getStartedButton;

    public void enter_user_name(String username){
        usernameField.type(username);
    }

    public void enter_password(String password){
        passwordField.type(password);
    }

    public void click_login_button(){
        loginButton.click();
        waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),'Welcome') or contains(text(),'Please')]")));
    }

    public void click_create_account_link(){
        createAccountLink.click();
    }

    public boolean login_button_is_displayed(){
        return loginButton.isDisplayed();
    }
}
