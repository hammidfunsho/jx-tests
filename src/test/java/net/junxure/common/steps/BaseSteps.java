package net.junxure.common.steps;

import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class BaseSteps {

    @Steps
    private BaseUserSteps baseUserSteps;

    @When("^I click on the \"(.*)\" (.*)$")
    public void i_click_on_element_with_text(String text, String tag) {
        baseUserSteps.click_on_element_with_exact_text(tag, text);
    }


}
