package net.junxure.common.steps;

import net.junxure.common.pages.BasePage;
import net.junxure.common.pages.LoginPage;
import net.junxure.utils.ColumnsWithNumbers;
import net.junxure.utils.ColumnsWithDates;
import net.thucydides.core.annotations.Step;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BaseUserSteps {

    private BasePage currentPage;
    private LoginPage loginPage;
    
    /*
        Navigation Steps
     */

    @Step
    public void navigate_to_homepage() {
        loginPage.open();
    }

    /*
        Authentication Steps
    */

    @Step
    public void login_with_password(String username, String password) {
        loginPage.enter_user_name(username);
        loginPage.enter_password(password);
        loginPage.click_login_button();
    }

    @Step
    public void enter_username(String username) {
        loginPage.enter_user_name(username);
    }

    @Step
    public void enter_password(String password) {
        loginPage.enter_password(password);
    }

    /* 
        Assertions
    */

    @Step
    public void shouldSeetext(String message) {
        assertThat(currentPage.containsText(message)).isTrue();
    }

    @Step
    public void shouldNotSeeText(String words) {
        assertThat(currentPage.containsAllText(words)).isFalse();
    }

    @Step
    public void should_see_text_ignoring_case(String words) {
        assertThat(currentPage.contains_text_ignoring_case(words)).isTrue();
    }

    @Step
    public void should_not_see_text_ignoring_case(String words) {
        assertThat(currentPage.contains_text_ignoring_case(words)).isFalse();
    }

    @Step
    public void verify_list_is_sorted_by_column_and_order(String sortOrder, String columnName, List<String> actualListOfSortableItems) {

        assertThat(actualListOfSortableItems.size()).
                as(String.format("There are no items to sort in %s order by %s column or " +
                        "sortable items are empty strings", sortOrder, columnName)).
                isGreaterThan(0);

        if (ColumnsWithNumbers.column_contains_numbers(columnName)) {
            currentPage.verify_list_is_sorted_by_column_and_order_for_numbers(sortOrder, actualListOfSortableItems);
        } else if (ColumnsWithDates.column_contains_dates(columnName)) {
            currentPage.verify_list_is_sorted_by_column_and_order_for_dates(sortOrder, actualListOfSortableItems);
        } else {
            currentPage.verify_list_is_sorted_by_column_and_order_for_string_values(sortOrder, actualListOfSortableItems);
        }
    }
    
    @Step
    public void click_on_element_with_exact_text(String tag, String text){
        currentPage.click_on_element_with_exact_text(tag, text);
    }
}