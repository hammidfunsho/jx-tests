package net.junxure.jxcloud.pages;

import net.junxure.common.pages.BasePage;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

public class BasePlatformPage extends BasePage {

    @FindBy(id = "icon-power-button")
    private WebElementFacade logoutButton;

    public void logout() {
        logoutButton.click();
    }

    public void select_module(String moduleName) {
        WebElementFacade platformModule = find(By.xpath("//*[starts-with(@class,'main-menu__item')]/a[contains(text(), '" + moduleName + "')] | //*[starts-with(@class,'TopNavLink')][contains(text(), '" + moduleName + "')]"));
        platformModule.click();
    }

    public void select_sub_module(String subModuleName) {
        WebElementFacade platformSubModule =
                find(By.xpath("//*[contains(@class,'sub-menu_active')]//a[contains(text(), '" + subModuleName + "')]"));
        platformSubModule.click();
    }
}
