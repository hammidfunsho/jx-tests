package net.junxure.jxcloud.pages;

import net.junxure.common.pages.BasePage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://junxurecloud.com")
public class JunxureLogonPage extends BasePage {

    @FindBy(id = "UserName")
    private WebElementFacade usernameField;

    @FindBy(id = "Password")
    private WebElementFacade passwordField;

    @FindBy(xpath = ".//*[@class='btn btn-large']")
    private WebElementFacade loginButton;

    public void clickLoginButton(){
        loginButton.click();
    }

    public void typeUsername(String username){
        usernameField.type(username);
    }

    public void typePassword(String password){
        passwordField.type(password);
    }

}
