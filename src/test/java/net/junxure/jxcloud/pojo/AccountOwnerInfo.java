package net.junxure.jxcloud.pojo;

import lombok.Getter;
import lombok.Setter;
import net.junxure.utils.ModelTransformer;

@Setter
@Getter
public class AccountOwnerInfo extends ModelTransformer {
    private String firstName;
    private String middleName = "David";
    private String lastName;
    private String emailAddress;
    private String phone = "1234567890";
    private String homeStreetAddress = "10753 blix";
    private String zip = "90002";
    private String city = "Syracuse";
    private String state = "CA";
    private Boolean haveDifferentMailingAddress = false;
    private String citizenship = "U.S. Citizen";
    private String ssn;
    private String dateOfBirth = "03/16/1954";
    private String countryOfBirth = "Armenia";
    private String maritalStatus = "Single";
    private String numberOfDependents = "0";
    private String driversLicenceNumber = "130838713";
    private String driversLicenceExpirationDate = "03/11/2019";
    private String driversLicenceState = "CA";
    private String annualIncome = "$50,000-$99,999";
    private String approximateNetWorth = "$100,000-$249,999";
    private String employment = "Retired";
    private String sourceOfIncome = "Savings";
    private String initialSourceOfFunds = "Lottery/Gambling";
    private String ongoingSourceOfFunds = "Savings";
    private Boolean isAffiliated = false;
    private Boolean isDirector = false;
    private String trustedContactFirstName = "Thierry";
    private String trustedContactMiddleName = "Ars";
    private String trustedContactLastName = "Henry";
    private String trustedContactRelationshipToAccountOwner = "Friend";
    private String trustedContactEmailAddress = "henry@gmail.com";
    private String trustedContactMailingAddress = "10753 blix";
    private String trustedContactPhone = "1234567890";
    private String trustedContactHomeStreetAddress = "10753 blix";
    private String trustedContactZip = "90002";
    private String trustedContactCity = "London";
    private String trustedContactState = "CA";
    private Boolean hasBeneficiaries = false;
}