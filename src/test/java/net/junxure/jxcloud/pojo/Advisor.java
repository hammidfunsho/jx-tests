package net.junxure.jxcloud.pojo;

import lombok.Getter;
import lombok.Setter;
import net.junxure.utils.ModelTransformer;
import org.apache.commons.lang3.RandomStringUtils;

@Setter
@Getter
public class Advisor extends ModelTransformer {
    private String firstName;
    private String lastName;
    private String email = "emailbox".concat(RandomStringUtils.randomAlphabetic(10)).concat("@gmail.com");
    private String phoneNumber = "6664446666";
    private String faxNumber = "6664446666";
    private String displayName;
    private String firmName;
    private String twitterHandle = "https://www.twitter.com";
    private String address = "646464 Acme way";
    private String jointAdvisorEmail = "jharrodworkemail@workemal.com";
    private String linkedIn = "https://www.linkedIn.com";
    private String zip = "77083";
    private String city = "Houston";
    private String facebook = "https://www.facebook.com";
    private String websiteAddress = "646464 Acme way";
    private String serviceRep = "646464 Acme way";
    private String startDate = "646464 Acme way";
    private String endDate = "646464 Acme way";

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}