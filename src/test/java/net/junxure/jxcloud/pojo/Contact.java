package net.junxure.jxcloud.pojo;

import lombok.Getter;
import lombok.Setter;
import net.junxure.utils.ModelTransformer;

@Setter
@Getter
public class Contact extends ModelTransformer {

    private String firstName;
    private String middleInitial;
    private String lastName;
    private String advisorCode = "099";
    private String emailAddress = "jharrod@hdhdhd.com";
    private String workEmail = "jharrodworkemail@workemal.com";
    private String facebook = "https://www.facebook.com";
    private String twitter = "https://www.twitter.com";
    private String phoneNumber = "6664446666";
    private String extension = "123";
    private String workPhoneNumber = "8388383838";
    private String workExtension = "543";
    private String linkedIn = "https://www.linkedIn.com";
    private String googlePlus = "https://plus.google.com";
    private String homeAddress = "1234 Address This Way";
    private String homeZip = "77083";
    private String homeCity = "Houston";
    private String companyName = "Acme Inc";
    private String companyAddress = "646464 Acme way";
    private String companyZip = "78737";
    private String companyCity = "Houston";
}