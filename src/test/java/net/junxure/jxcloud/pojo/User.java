package net.junxure.jxcloud.pojo;

import lombok.Getter;
import lombok.Setter;
import net.junxure.utils.ModelTransformer;

@Setter
@Getter
public class User extends ModelTransformer {
    private String firstName;
    private String lastName;
    private String workEmail;
    private String workPhone;
    private String anotherEmail = "qa@test.com";
    private String anotherPhone = "12345678901234";
    private String username = "username";
    private String assignedRole;
    private String assignedAdvisorCode;
    private String salutation ="Mr." ;
    private String birthday = "02/08/1984";
    private String companyName = "QA Company";
    private String companyAddress = "10753 blix";
    private String companyZip = "91602";
    private String companyZipExtension = "4444";
    private String companyCity = "QA city";
    private String companyState = "CA";
    private String homeAddress = "10753 blix";
    private String homeZip = "91602";
    private String homeZipExtension = "4444";
    private String homeCity = "QA city";
    private String homeState = "CA";
    private boolean isActiveUser = true;
    private boolean isUnlimitedLoginAttemptsAllowed = false;

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}