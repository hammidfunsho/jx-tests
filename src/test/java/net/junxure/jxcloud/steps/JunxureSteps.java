package net.junxure.jxcloud.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.junxure.common.steps.BaseUserSteps;
import net.junxure.jxcloud.steps.platform.JunxureUserSteps;
import net.thucydides.core.annotations.Steps;

public class JunxureSteps extends BaseUserSteps {

    private final String adminUserName = "db@365.com";
    private final String adminPassword = "test1234";

    @Steps
    private JunxureUserSteps junxureUserSteps;

    @Given("^I am on the Junxure Login page$")
    public void goToJunxureLoginPage() {
        junxureUserSteps.navigateToJunxureLogonPage();
    }

    @And("^I log in as a Junxure Admin")
    public void loginAsAdmin(){
        junxureUserSteps.loginToJunxure(adminUserName, adminPassword);
    }

    @And("^I log in with the username '(.*)' and password '(.*)'")
    public void loginWithUsernameAndPassword(String username, String password){
        junxureUserSteps.loginToJunxure(username, password);
    }

    /*
    Verification Steps
     */

    @Then("^I should see the words? '(.*)'")
    public void shouldSeeTheWords(String message) {
      junxureUserSteps.shouldSeetext(message);
    }

    @Then("^I should not see the words? '(.*)'")
    public void shouldNotSeeWords(String message) {
        junxureUserSteps.shouldNotSeeText(message);
    }


}
