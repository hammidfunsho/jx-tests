package net.junxure.jxcloud.steps.platform;

import net.junxure.common.steps.BaseUserSteps;
import net.junxure.jxcloud.pages.JunxureLogonPage;
import net.thucydides.core.annotations.Step;

public class JunxureUserSteps extends BaseUserSteps {

    private JunxureLogonPage junxureLogonPage;

    @Step
    public void navigateToJunxureLogonPage(){
        junxureLogonPage.open();
    }

    @Step
    public void loginToJunxure(String username, String password){
        junxureLogonPage.open();
        junxureLogonPage.typeUsername(username);
        junxureLogonPage.typePassword(password);
        junxureLogonPage.clickLoginButton();
    }

}
