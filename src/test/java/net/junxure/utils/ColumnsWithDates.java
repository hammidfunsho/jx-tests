package net.junxure.utils;
import org.apache.commons.lang3.EnumUtils;

public enum ColumnsWithDates {
    RUN_BUSINESS_USER_MANAGEMENT_DATE_CREATED_COLUMN("Date Created");

    private final String name;
    ColumnsWithDates(String name) {
        this.name = name;
    }

    public static boolean column_contains_dates(String name) {
        return EnumUtils.isValidEnum(ColumnsWithDates.class, name);
    }
}