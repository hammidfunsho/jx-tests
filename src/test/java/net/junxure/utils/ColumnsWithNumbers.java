package net.junxure.utils;
import org.apache.commons.lang3.EnumUtils;

public enum ColumnsWithNumbers {

    GROW_PRACTICE_DIGITAL_ADVISE_TABLE_PROGRESS_COLUMN("Progress"),
    GROW_PRACTICE_DIGITAL_ADVISE_TABLE_RISK_TOLERANCE_COLUMN("Risk Tolerance"),
    MANAGE_CLIENTS_DASHBOARD_ACCOUNT_TABLE_MARKET_VALUE_COLUMN("Market Value");

    private final String name;

    ColumnsWithNumbers(String name) {
        this.name = name;
    }

    public static boolean column_contains_numbers(String name) {
        return EnumUtils.isValidEnum(ColumnsWithDates.class, name);
    }
}
