package net.junxure.utils;

import lombok.extern.slf4j.Slf4j;
import java.lang.reflect.Field;

@Slf4j
public abstract class ModelTransformer {

    public static <T> T transform(T mappedModel) {
        T defaultModel = null;
        try {
            defaultModel = (T) mappedModel.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log.error(e.getMessage());
        }
        Field[] fields = defaultModel.getClass().getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                Object mappedValue = field.get(mappedModel);
                if(!(mappedValue == null)){
                    field.set(defaultModel, mappedValue);
                }
            }
        } catch (IllegalAccessException e) {
            log.error(e.getMessage());
        }
        return defaultModel;
    }
}