package net.junxure.utils;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * The AUT sorting functionality relies on the default Postgres sorting mechanism, which differs from Java in its implementation.
 * In order to account for these variations, we’ve implemented a custom comparator which normalizes the comparisons”
 * In order to apply those algorithms in java custom comparator was added
 */
public class PostgresStringComparator implements Comparator<String> {

    @Override
    public int compare(String left, String right) {
        Collator collator = Collator.getInstance(Locale.US);
        collator.setStrength(Collator.PRIMARY);
        return collator.compare(left.replaceAll("\\p{Punct}", ""), right.replaceAll("\\p{Punct}", ""));
    }
}