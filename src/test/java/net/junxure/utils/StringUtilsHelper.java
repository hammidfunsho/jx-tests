package net.junxure.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Utils class is created for converting List of Strings which we get from UI
 * into proper data type for sorting purposes
 */
public class StringUtilsHelper {

    private StringUtilsHelper() {
    }

    /**
     * Salutations are ignored on UI when sorting is performed
     * Thus ones should be replaced in order to verify that the list of Strings is sorted
     * in accordance to the supplied comparator
     */
    public static List<String> remove_salutation(List<String> listOfItems) {
        List<String> listOfSortableStrings = new ArrayList<>();
        listOfItems.forEach(sortableItem -> listOfSortableStrings.
                add(sortableItem.
                        replace("Mr. ", "").
                        replace("Ms. ", "").
                        replace("Mrs. ", "").
                        replace("Dr. ", "")
                ));
        return listOfSortableStrings;

    }
    
    /**
     * In order to sort by numbers we should convert the List of Strings into the 
     * list of Doubles or list of Integers. 
     * To do this we need to get rid of extra characters like : % and ,
     */
    public static List<Double> remove_percentage_and_comma_sign(List<String> listOfItems) {
        List<Double> listOfSortableNumbers = new ArrayList<>();
        listOfItems.forEach(sortableItem -> {
            listOfSortableNumbers.add(Double.parseDouble(
                    sortableItem.
                            replace(",", "").
                            replace("%", "")));
        });
        return listOfSortableNumbers;

    }
    
    /**
     * In order to sort by dates we should convert the List of Strings into the 
     * list of Dates.
     * To do this we need to supply the correct date pattern in accordance to date format on UI
     */
    public static List<LocalDate> convert_list_of_strings_to_list_of_dates(List<String> listOfItems) {
        List<LocalDate> listOfSortableDates = new ArrayList<>();
        listOfItems.forEach(sortableItem -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            listOfSortableDates.add(LocalDate.parse(sortableItem, formatter));
        });
        return listOfSortableDates;
    }
}