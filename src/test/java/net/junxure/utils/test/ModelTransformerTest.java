package net.junxure.utils.test;

import net.junxure.jxcloud.pojo.Contact;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class ModelTransformerTest {

    public static String NON_DEFAULT_FIRST_NAME = "Joe";
    public static String NON_DEFAULT_LAST_NAME = "Leedsea";
    public static String NON_DEFAULT_ZIP_CODE = "00000";
    public static String DEFAULT_TWITTER = "https://www.twitter.com";
    public static String DEFAULT_ADVISOR_CODE = "099";

    @Test
    public void testTransform(){
        Contact initialContact = new Contact();
        initialContact.setFirstName(NON_DEFAULT_FIRST_NAME);
        initialContact.setLastName(NON_DEFAULT_LAST_NAME);
        initialContact.setHomeZip(NON_DEFAULT_ZIP_CODE);
        Contact updatedContact = Contact.transform(initialContact);

        assertTrue(initialContact.getFirstName().equals(NON_DEFAULT_FIRST_NAME));
        assertTrue(initialContact.getLastName().equals(NON_DEFAULT_LAST_NAME));
        assertTrue(initialContact.getHomeZip().equals(NON_DEFAULT_ZIP_CODE));

        assertTrue(updatedContact.getFirstName().equals(NON_DEFAULT_FIRST_NAME));
        assertTrue(updatedContact.getLastName().equals(NON_DEFAULT_LAST_NAME));
        assertTrue(updatedContact.getHomeZip().equals(NON_DEFAULT_ZIP_CODE));

        assertTrue(updatedContact.getTwitter().equals(DEFAULT_TWITTER));
    }

    @Test
    public void testTransformWithNullValues(){
        Contact initialContact = new Contact();
        initialContact.setAdvisorCode(null);
        Contact updatedContact = Contact.transform(initialContact);

        assertTrue(initialContact.getAdvisorCode() == null);
        assertTrue(updatedContact.getAdvisorCode().equals(DEFAULT_ADVISOR_CODE));
    }
}