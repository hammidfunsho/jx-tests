@jxauthentication
@jxregression
@jxsmoke

Feature: User Authentication

  Scenario Outline: Log in with invalid user credentials
    Given I am on the Junxure Login page
    And I log in with the username '<username>' and password '<password>'
    Then I should see the words 'Login was unsuccessful. Please correct the errors and try again.'
    Examples:
      | username | password |
      | John     | doe      |
      | Mike     | smith    |