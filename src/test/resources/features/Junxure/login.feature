@jxauthentication
@jxregression
@jxsmoke1

Feature: User Authentication
        As an admin, I want to be able log into Junxure Cloud

  Scenario: Log in with to Junxure
    Given I am on the Junxure Login page
    And I log in as a Junxure Admin
    Then I should not see the words 'error'
